using Bitcoin.Services.Interfaces;
using Bitcoin.Services.Services;
using Polly;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<IBitcoinService, BitcoinService>();
builder.Services.AddHttpClient("Bitcoin", client =>
{
    client.BaseAddress = new Uri("https://api.coindesk.com/v1/bpi/currentprice.json");
}).AddTransientHttpErrorPolicy(x => x.WaitAndRetryAsync(3, timeInterval => TimeSpan.FromMilliseconds(333)));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
