﻿using Bitcoin.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BitcoinPriceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BitcoinController : ControllerBase
    {
        private readonly IHttpClientFactory clientFactory;
        private readonly IBitcoinService bitcoinService;

        public BitcoinController(IBitcoinService bitcoinService, IHttpClientFactory clientFactory)
        {
            this.bitcoinService = bitcoinService;
            this.clientFactory = clientFactory;
        }

        // GET: api/BitcoinController
        [HttpGet]
        public async Task<IEnumerable<string>> Get()
        {            
            return (await this.bitcoinService.ReturnResponseData(this.clientFactory
                 .CreateClient("Bitcoin"), null)).Select(p => p.ToString());
        }

        // GET api/BitcoinController/5
        [HttpGet("{currency}")]
        public async Task<string> Get(string currency)
        {
             return this.bitcoinService.FindAvgPrice(await this.bitcoinService
                 .ReturnResponseData(this.clientFactory
                 .CreateClient("Bitcoin"),currency))
                .ToString();            
        }      
    }
}
