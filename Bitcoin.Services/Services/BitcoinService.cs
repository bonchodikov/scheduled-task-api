﻿using Bitcoin.Services.Interfaces;
using Bitcoin.Services.Models;
using System.Net.Http.Json;

namespace Bitcoin.Services.Services
{
    public class BitcoinService : IBitcoinService
    {
        private readonly PeriodicTimer timer = new(TimeSpan.FromMinutes(1));
        private readonly CancellationTokenSource token = new();
        private readonly Task? timerTsk;

        private const byte NUMBER_OF_RETRIVES = 5;

        private bool Guard(string stringToVerify)
        {
            if (string.IsNullOrEmpty(stringToVerify) ||
                string.IsNullOrWhiteSpace(stringToVerify)
                || !stringToVerify.ToUpper().Equals("EUR")
                || !stringToVerify.ToUpper().Equals("GBP")
                || !stringToVerify.ToUpper().Equals("USD"))
            {
                return true;
            }
            return false;
       }
        public async Task<BitcoinModel> GetDataPerCurrency(HttpClient client, string currency)
        {
            var address = client.BaseAddress.AbsoluteUri;

            if (!this.Guard(currency))
            {
                 address = address.Replace(".json", "/" + currency + ".json");

            }           
            var request = new HttpRequestMessage(HttpMethod.Get, address);
            HttpResponseMessage response = await client.SendAsync(request);
            if (response.IsSuccessStatusCode && response is not null)
            {
                var result = await response.Content.ReadFromJsonAsync<BitcoinModel>();
                result.Currency = currency;
                return result;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public AvgBitcoinPriceModel FindAvgPrice(IEnumerable<BitcoinModel> bitcoinModels)
        {
            return this.CreateResult(bitcoinModels);
        }
        
        private AvgBitcoinPriceModel CreateResult(IEnumerable<BitcoinModel> bitcoinModels)
        {
            /// Better solution would be to implement custom json serializator in order to have only one class 'Currency' instead of N - usd, gbp, eur etc. 
            
            switch (bitcoinModels.FirstOrDefault()?.Currency.ToLower())
            {
                case "usd": return new AvgBitcoinPriceModel { AvgPrice = bitcoinModels
                    .Select(p => (decimal)p.Bpi.USD.rate_float).Average(),
                    Currency = "USD",
                    HighestPrice = (decimal) bitcoinModels.Select(x=>x.Bpi.USD.rate_float).Max() };
                case "gbp":
                    return new AvgBitcoinPriceModel
                    {
                        AvgPrice = bitcoinModels
                .Select(p => (decimal)p.Bpi.GBP.rate_float).Average(),
                        Currency = "GBP",
                        HighestPrice = (decimal)bitcoinModels.Select(x => x.Bpi.GBP.rate_float).Max()
                    };
                default:
                    return new AvgBitcoinPriceModel
                    {
                        AvgPrice = bitcoinModels
                .Select(p => (decimal)p.Bpi.EUR.rate_float).Average(),
                        Currency = "EUR",
                        HighestPrice = (decimal)bitcoinModels.Select(x => x.Bpi.EUR.rate_float).Max()
                    };
            }
        }
        public async Task<IEnumerable<BitcoinModel>> ReturnResponseData(HttpClient client, string currency)
        {
            var collection = new List<BitcoinModel>();
            int index = 0;
            while (index < NUMBER_OF_RETRIVES )
            {
                collection.Add(await GetDataPerCurrency(client, currency));
                if (index+1<NUMBER_OF_RETRIVES)
                {
                    await this.timer.WaitForNextTickAsync(token.Token);
               }
                index++;

            }
            await this.StopAsynv();
            return  collection;
        }

        private async Task StopAsynv()
        {
            if (this.timerTsk is null)
            {
                return;
            }
            this.token.Cancel();
            await this.timerTsk;
            this.token.Dispose();
            Console.WriteLine("Task was canceled");
        }
    }
}
