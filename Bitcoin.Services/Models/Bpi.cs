﻿namespace Bitcoin.Services.Models
{
    public class Bpi
    {
        public EUR EUR { get; set; }
        public GBP GBP { get; set; }
        public USD USD { get; set; }
    }

}
