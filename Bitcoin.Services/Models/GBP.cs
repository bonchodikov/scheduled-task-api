﻿namespace Bitcoin.Services.Models
{
    public class GBP
    {
        public string code { get; set; }
        public string rate { get; set; }
        public string description { get; set; }
        public float rate_float { get; set; }
    }

}
