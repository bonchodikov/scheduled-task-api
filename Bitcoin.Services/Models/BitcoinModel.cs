﻿namespace Bitcoin.Services.Models
{
    public class BitcoinModel
    {
        public Time Time { get; set; }
        public string Disclaimer { get; set; }
        public Bpi Bpi { get; set; }
        public string  Currency { get; set; }

        public override string ToString()
        {
            return $"Price for {this.Bpi.EUR.code} " + Math.Round(this.Bpi.EUR.rate_float,2);
        }
    }

}
