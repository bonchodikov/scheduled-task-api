﻿namespace Bitcoin.Services.Models
{
    public class AvgBitcoinPriceModel
    {
        public decimal AvgPrice { get; set; }
        public decimal HighestPrice { get; set; }
        public string Currency { get; set; }

        public override string ToString()
        {
            return $"Average Price for {this.Currency} is: " + this.AvgPrice.ToString() +
                Environment.NewLine + 
                $"Highest Price for {this.Currency} is: "+this.HighestPrice.ToString(); 
        }
    }
}
