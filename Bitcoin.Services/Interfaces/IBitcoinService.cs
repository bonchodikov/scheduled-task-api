﻿using Bitcoin.Services.Models;

namespace Bitcoin.Services.Interfaces
{
    public interface IBitcoinService
    {
         Task<BitcoinModel> GetDataPerCurrency(HttpClient client, string currency);
        Task<IEnumerable<BitcoinModel>> ReturnResponseData(HttpClient client, string currency);
        AvgBitcoinPriceModel FindAvgPrice(IEnumerable<BitcoinModel> bitcoinModels);


    }
}
